package utfpr.persistence.controller;

import inscricao.persistence.entity.Candidato;
import inscricao.persistence.entity.Idioma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * Desenvolvimento de aplicações Web
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class CandidatoJpaController extends JpaController {

    public CandidatoJpaController() {
    }

    public List<Candidato> findbyIdioma() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c INNER JOIN c.idioma i ORDER BY i.descricao, c.nome",
                    Candidato.class);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidato> findbyIdioma(Integer codigo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c INNER JOIN c.idioma i WHERE i.codigo = :codigo ORDER BY i.descricao, c.cpf",
                    Candidato.class);
            q.setParameter("codigo", codigo);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidato> findbyIdioma(Idioma idioma) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c INNER JOIN c.idioma i WHERE i.codigo = :codigo ORDER BY i.descricao, c.cpf",
                    Candidato.class);
            q.setParameter("codigo", idioma.getCodigo());
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
